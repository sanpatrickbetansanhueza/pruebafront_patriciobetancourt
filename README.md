**Prueba Front End**

Seguir las instrucciones para leventar prueba:

**

---

## Para Correr Prueba Técnica

Por favor seguir instrucciones a continuación.

1. Clonar proyecto
2. Posionarse en la rama master
3. Ejecutar para instalar dependencias: npm i
4. Para levantar prueba, ejecutar: npm run start
5. En el navegador, ingresar a la url: http://localhost:4200
6. Para revisar cobertura pruebas unitarias, ejecutar: npm run test

--- 