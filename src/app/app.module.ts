import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { LoginComponent } from './components/login/login.component';
import { ListarHistoriasComponent } from './components/listar-historias/listar-historias.component';
import { NavigationComponent } from './components/navigation/navigation.component';

import { LoginService } from './services/login.service';
import { ListarHistoriasService } from './services/listar-historias.service';

import { FormsModule, ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavigationComponent,
    ListarHistoriasComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule, 
  ],
  providers: [
    LoginService,
    ListarHistoriasService,
  ],
  bootstrap: [AppComponent] 
})
export class AppModule { }
