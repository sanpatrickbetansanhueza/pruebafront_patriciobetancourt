import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient,HttpClientModule,HttpResponse } from '@angular/common/http'
import { RouterTestingModule } from '@angular/router/testing';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpEventType } from '@angular/common/http';

import { LoginService } from './login.service';
import { Login } from '../models/Login';

describe('LoginService', () => {

  let loginService: LoginService;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpClientModule,HttpClientTestingModule],
        providers: [LoginService]
    });

    loginService = TestBed.inject(LoginService);
    httpTestingController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

  });

  afterEach(() => {
    httpTestingController.verify(); //Verifica que no haya solicitudes pendientes
  }); 

  //Caso 1
  it('Debe loguearse y devolver respuesta', () => {

    const newLogin: Login = { username: 'prueba1', password: 'prueba1', type: 'normal' };

    loginService.getLogin(newLogin).subscribe(
      data => expect(data).toEqual(newLogin, 'Debe retonar login'));

    const req = httpTestingController.expectOne('https://taiga.tecnoandina.cl/api/v1/auth');
    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Logueado', body: newLogin });
    expect(expectedResponse.status).toBe(201); 
  });


  //Caso 2
  it('Debe retornar caso errado, error 500', () => {
    const newLogin: Login = { username: 'prueba1', password: 'prueba1', type: 'normal' };

    loginService.getLogin(newLogin).subscribe(
      data => expect(data).toEqual(newLogin, 'Debe retornar login'));

    const req = httpTestingController.expectOne('https://taiga.tecnoandina.cl/api/v1/auth');

    const expectedResponse = new HttpResponse({ status: 500, statusText: 'Error Servidor', body: newLogin });
    expect(expectedResponse.status).toBe(500); 
  });

});
