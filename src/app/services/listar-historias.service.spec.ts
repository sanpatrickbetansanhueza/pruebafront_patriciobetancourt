import { TestBed, async, inject } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { HttpClient,HttpClientModule,HttpResponse } from '@angular/common/http'
import { RouterTestingModule } from '@angular/router/testing';
import { Injectable } from '@angular/core';
import { HttpEvent, HttpEventType } from '@angular/common/http';

import { ListarHistoriasService } from './listar-historias.service';

describe('ListarHistoriasService', () => {

  let listarHistoriasService: ListarHistoriasService;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;

  beforeEach(() => {
    TestBed.configureTestingModule({
        imports: [RouterTestingModule, HttpClientModule,HttpClientTestingModule],
        providers: [ListarHistoriasService]
    });

    listarHistoriasService = TestBed.inject(ListarHistoriasService);
    httpTestingController = TestBed.inject(HttpTestingController);
    httpClient = TestBed.inject(HttpClient);

  });

  afterEach(() => {
    httpTestingController.verify(); //Verifica que no haya solicitudes pendientes
  }); 

  /*it('should be created', () => {
    expect(listarHistoriasService).toBeTruthy();
  });*/

  //Caso 1
  it('Debe retornar historias usuario', () => {

    listarHistoriasService.listarHistorias().subscribe(
      data => expect(data).toEqual('Debe retonar hiorias'));

    const req = httpTestingController.expectOne('https://taiga.tecnoandina.cl/api/v1/userstories');

    const expectedResponse = new HttpResponse({ status: 201, statusText: 'Logueado', body: 'historias'});
    expect(expectedResponse.status).toBe(201); 
  });

});