import { Injectable } from '@angular/core';
import { HttpClient,HttpClientModule } from '@angular/common/http'
import { Login } from '../models/Login';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators'; 

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  	API_URI = 'https://taiga.tecnoandina.cl/api/v1'; 

  	constructor(private http: HttpClient) { }

    getLogin(login:Login){
    	return this.http.post(`${this.API_URI}/auth`, login);
    }

}