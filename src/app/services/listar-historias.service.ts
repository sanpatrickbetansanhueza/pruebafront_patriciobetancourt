import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http'
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ListarHistoriasService {

	API_URI = 'https://taiga.tecnoandina.cl/api/v1';

  	constructor(private http: HttpClient) { }

  	listarHistorias(){
  		let auth_token = localStorage.getItem('auth_token');
  		let headers = new HttpHeaders();
		headers = headers.set('Content-Type','application/json').set('Authorization','Bearer '+auth_token);
	    return this.http.get(`${this.API_URI}/userstories`, {headers: headers});
	}
  }
 