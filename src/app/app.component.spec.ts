import { TestBed, async } from '@angular/core/testing';
import { AppComponent } from './app.component';
import { createComponentFactory, Spectator } from '@ngneat/spectator/jest';

import { NavigationComponent } from './components/navigation/navigation.component';

import {RouterTestingModule} from '@angular/router/testing';

import { LoginComponent } from './components/login/login.component';
import { ListarHistoriasComponent } from './components/listar-historias/listar-historias.component';

/*
describe('AppComponent', () => {
  const createComponent = createComponentFactory({
    component: AppComponent
  });
  let spectator: Spectator<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
          AppComponent,
          NavigationComponent
      ],
      imports: [RouterTestingModule.withRoutes(
          [
            {
              path: '',
              redirectTo : '/login',
              pathMatch: 'full'
            },
            {
              path: 'login',
              component : LoginComponent
            },
            {
              path:'listar-historias',
              component: ListarHistoriasComponent
            },
          ]
      )
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => spectator = createComponent());

  it('should create the app', () => {
    const app = spectator.component;
    expect(app).toBeTruthy();
  });
  
});


*/

describe('AppComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent,
        NavigationComponent
      ],
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  /*
  it(`should have as title 'prueba'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('prueba');
  });

  it('should render title', () => {
    const fixture = TestBed.createComponent(AppComponent);
    fixture.detectChanges();
    const compiled = fixture.nativeElement;
    expect(compiled.querySelector('.content span').textContent).toContain('prueba app is running!');
  });*/
});
