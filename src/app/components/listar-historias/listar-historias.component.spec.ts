import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ListarHistoriasComponent } from './listar-historias.component';

import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';

describe('ListarHistoriasComponent', () => {
  let component: ListarHistoriasComponent;
  let fixture: ComponentFixture<ListarHistoriasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListarHistoriasComponent ],
      imports:[RouterTestingModule, HttpClientTestingModule,ReactiveFormsModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListarHistoriasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
