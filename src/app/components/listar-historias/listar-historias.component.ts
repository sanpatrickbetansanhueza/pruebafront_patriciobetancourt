import { Component, OnInit } from '@angular/core';
import { ListarHistoriasService} from '../../services/listar-historias.service';
import { ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-listar-historias',
  templateUrl: './listar-historias.component.html',
  styleUrls: ['./listar-historias.component.css']
})
export class ListarHistoriasComponent implements OnInit {

	photo:string = '';
	username:string = '';
	historias: any = [];

  	constructor(private listarHistoriasService: ListarHistoriasService,private router: Router) { 
  		this.photo = localStorage.getItem('photo');
  		this.username = localStorage.getItem('username');
  	}

	ngOnInit(): void {
	  	this.listarHistorias();
	}

  	listarHistorias(){
	    this.listarHistoriasService.listarHistorias().subscribe(
	      res =>  {
	        if(res){
	        	this.historias = res;
	        	console.log(this.historias);
	        }
	      },
	      err => {
	      	console.log('Error',err);
	      	console.error(err);
	      	this.router.navigate(['/login']);
	      }
	    );
  	}

}
