import { async, ComponentFixture, TestBed, inject  } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule,HttpTestingController } from '@angular/common/http/testing';
import { ReactiveFormsModule } from '@angular/forms';
import { HttpClient,HttpClientModule,HttpResponse } from '@angular/common/http'
import { Injectable } from '@angular/core';
import { HttpEvent, HttpEventType } from '@angular/common/http';

import { LoginComponent } from './login.component';

const filterTestFn = jest.fn();

describe('LoginComponent', () => {

  let fixture: ComponentFixture<LoginComponent>;

  let loginComponent: LoginComponent;
  let httpTestingController: HttpTestingController;
  let httpClient: HttpClient;
  let myServiceMock;

  beforeEach(async(() => {

    TestBed.configureTestingModule({
        declarations: [ LoginComponent ],
        imports: [RouterTestingModule, HttpClientModule,HttpClientTestingModule,ReactiveFormsModule],
    })
    .compileComponents();
  }));

  beforeEach(() => {

    myServiceMock = {
        getLogin: jest.fn()
    }

    fixture = TestBed.createComponent(LoginComponent);
    loginComponent = fixture.componentInstance;
    fixture.detectChanges();

  });

  it('should create', () => {
    expect(loginComponent).toBeTruthy();
  }); 

  test('spyOn .getLogin()', () => {
    const somethingSpy = jest.spyOn(loginComponent, 'getLogin');
    loginComponent.getLogin();
    expect(somethingSpy).toBeTruthy();
  });

  //--------------------------------------------------------------------------------

  test('spyOn .validarDatos()', () => {

    this.loginForm = {
      username : 'algo',
      password: 'algo'
    }

    this.validoUsername = true;
    this.validoPassword = true;

    const somethingSpy = jest.spyOn(loginComponent, 'validarDatos');
    loginComponent.validarDatos();
    expect(somethingSpy).toBeTruthy();
  });

  test('spyOn .validarDatos()', () => {

    const somethingSpy = jest.spyOn(loginComponent, 'validarDatos');
    loginComponent.validarDatos();
    expect(somethingSpy).toBeTruthy();
  });
  
});