import { Component, OnInit, HostBinding } from '@angular/core';
import { Login } from '../../models/Login';
import { LoginService} from '../../services/login.service';
import { ActivatedRoute, Router} from "@angular/router";
import { FormGroup,  FormBuilder,  Validators, ReactiveFormsModule } from '@angular/forms';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  login: Login = {  //Objeto a enviar por POST para realizar Login
    username: '', 
    password : '',
    type : 'normal'
  }

  validoUsername: boolean = true; //Flag para msjes de error
  validoPassword: boolean = true; //Flag para msjes de error
  validoDatosCorrectos: boolean = true; //Flag para msjes de error


  loginForm: FormGroup; //Formulario Login

  userName:string; //Se almacena valor del input en formulario
  passWord:string; //Se almacena valor del input en formulario

  constructor(private fb: FormBuilder,private loginService: LoginService,private router: Router, private activatedRoute: ActivatedRoute) {
    localStorage.clear();
    this.createForm();
  }

  ngOnInit(): void {
 
  }

  createForm() {
    this.loginForm = this.fb.group({
       username: ['', Validators.required ],
       password: ['', Validators.required ]
    });
  }

  getLogin(){

    this.loginService.getLogin(this.login).subscribe(
       res => {
          if(res['auth_token'] && res['auth_token'] !== ''){
            localStorage.setItem('auth_token',res['auth_token']);
            localStorage.setItem('username',res['username']);
            localStorage.setItem('photo',res['photo']);
            this.router.navigate(['/listar-historias']);
          }
       },
       err => { 
         console.log('Error:'); 
         console.log(err.error._error_message);
         this.validoDatosCorrectos = false;
         localStorage.clear();
        }

    );
  }

  validarDatos(){
    this.userName = this.loginForm.get('username').value;
    this.passWord = this.loginForm.get('password').value;

    if(!this.userName){
      this.validoUsername = false;
    }else{
      this.validoUsername = true;
      this.login.username = this.userName; //Llenando objeto a enviar por POST
    }
    if(!this.passWord){
      this.validoPassword = false;
    }else{
      this.validoPassword = true;
      this.login.password = this.passWord; //Llenando objeto a enviar por POST
    }

    if(this.validoUsername && this.validoPassword){
      this.getLogin();
    }
    
  }

}
